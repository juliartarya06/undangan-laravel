<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sambutan extends Model
{
    use SoftDeletes;
    protected $table = "sambutans";
    protected $guarded = [];
    public $with = ['agama'];

    public function agama(){
        return $this->belongsTo(Agama::class, 'agama_id');
    }

    public function pesanan()
    {
        return $this->hasMany(Pesanan::class, 'sambutan_id', 'id');
    }
}
