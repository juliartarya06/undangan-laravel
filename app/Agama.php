<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Agama extends Model
{
    use SoftDeletes;
    protected $table = "agamas";
    protected $guarded = [];

    public function sambutan(){
        return $this->hasMany(Sambutan::class, 'agama_id','id');
    }
}
