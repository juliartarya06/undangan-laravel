<?php

namespace App\Http\Controllers;

use App\Agama;
use App\Http\Requests\SambutanRequest;
use App\Sambutan;
use Illuminate\Http\Request;

class SambutanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Sambutan::get();
        return view("admin.sambutan.index", compact("data"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $agama = Agama::get();
        return view("admin.sambutan.create", compact("agama"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SambutanRequest $request)
    {
        Sambutan::insert($request->except('_token'));
        return redirect()->route('admin.sambutan.index')->with('success',$this->SUCCESS_ADD_MESSAGE);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sambutan  $sambutan
     * @return \Illuminate\Http\Response
     */
    public function show(Sambutan $sambutan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sambutan  $sambutan
     * @return \Illuminate\Http\Response
     */
    public function edit(Sambutan $sambutan)
    {
        $agama = Agama::get();
        return view("admin.sambutan.edit", compact("sambutan","agama"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sambutan  $sambutan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sambutan $sambutan)
    {
        Sambutan::find($sambutan->id)->update($request->all());
        return redirect()->route("admin.sambutan.index")->with("success",$this->SUCCESS_UPDATE_MESSAGE);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sambutan  $sambutan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sambutan $sambutan)
    {
        Sambutan::find($sambutan->id)->delete();
        return redirect()->route('admin.sambutan.index')->with("success", $this->SUCCESS_DELETE_MESSAGE);
    }
}
