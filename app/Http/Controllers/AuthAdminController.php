<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesAdmins;

class AuthAdminController extends Controller
{
    use AuthenticatesAdmins;
    protected $redirectTo = '/admin';

    // public function __construct()
    // {
    //     $this->middleware('guest:admin')->except('admin.logout');
    // }
}
