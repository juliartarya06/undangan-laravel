/*
SQLyog Enterprise v12.5.1 (64 bit)
MySQL - 10.4.11-MariaDB : Database - undangan_laravel
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `admins` */

DROP TABLE IF EXISTS `admins`;

CREATE TABLE `admins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `profile_picture` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `admins_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `admins` */

insert  into `admins`(`id`,`user_id`,`profile_picture`,`created_at`,`updated_at`,`deleted_at`) values 
(1,1,NULL,NULL,NULL,NULL);

/*Table structure for table `agamas` */

DROP TABLE IF EXISTS `agamas`;

CREATE TABLE `agamas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agama` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

/*Data for the table `agamas` */

insert  into `agamas`(`id`,`agama`,`created_at`,`updated_at`,`deleted_at`) values 
(5,'Hindu','2020-12-19 09:22:01','2020-12-19 01:22:01',NULL),
(6,'Islam','2020-12-19 02:42:33',NULL,NULL),
(7,'Budha','2020-12-19 02:42:42',NULL,NULL),
(8,'Kristen','2020-12-19 02:42:47',NULL,NULL);

/*Table structure for table `customers` */

DROP TABLE IF EXISTS `customers`;

CREATE TABLE `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `profile_picture` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `customers_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `customers` */

/*Table structure for table `failed_jobs` */

DROP TABLE IF EXISTS `failed_jobs`;

CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `failed_jobs` */

/*Table structure for table `hargas` */

DROP TABLE IF EXISTS `hargas`;

CREATE TABLE `hargas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `total_hari` int(11) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `hargas` */

insert  into `hargas`(`id`,`total_hari`,`harga`,`created_at`,`updated_at`,`deleted_at`) values 
(1,7,100000,'2020-12-19 12:08:20',NULL,NULL);

/*Table structure for table `images` */

DROP TABLE IF EXISTS `images`;

CREATE TABLE `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `undangan_id` int(11) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `status` enum('0','1') DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `images` */

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(1,'2014_10_12_000000_create_users_table',1),
(2,'2014_10_12_100000_create_password_resets_table',1),
(3,'2019_08_19_000000_create_failed_jobs_table',1);

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `sambutans` */

DROP TABLE IF EXISTS `sambutans`;

CREATE TABLE `sambutans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agama_id` int(11) DEFAULT NULL,
  `pembuka_title` varchar(255) DEFAULT NULL,
  `penutup_title` varchar(255) DEFAULT NULL,
  `pembuka_message` text DEFAULT NULL,
  `penutup_message` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `agama_id` (`agama_id`),
  CONSTRAINT `sambutans_ibfk_1` FOREIGN KEY (`agama_id`) REFERENCES `agamas` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Data for the table `sambutans` */

insert  into `sambutans`(`id`,`agama_id`,`pembuka_title`,`penutup_title`,`pembuka_message`,`penutup_message`,`created_at`,`updated_at`,`deleted_at`) values 
(1,5,'Om Swastyastu','Om Santi Santi Santi Om','Dengan Rasa hormat dask','Sekian dari saya termakasih Y','2020-12-19 09:56:58','2020-12-19 01:56:58',NULL),
(2,6,'Asalamualakum','Walaikum Salam','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut in risus eros. Quisque pellentesque rhoncus consectetur. Sed fringilla ante a velit dictum elementum. In semper maximus sapien non volutpat. Mauris lacus eros, luctus et hendrerit et, rhoncus at quam. Morbi tristique sapien in aliquam fringilla. Etiam non elementum purus, vitae varius odio.\r\n\r\nIn hendrerit imperdiet tortor sed consectetur. Nulla non dolor eu ex dapibus condimentum. Aenean congue, sapien vel tempus mattis, lacus ante ornare turpis, id efficitur felis orci id felis. Vestibulum sagittis sed turpis ac varius. Donec maximus tortor vitae orci aliquam ultricies id eget lectus. Donec vel pretium nulla. Ut blandit urna sed nibh commodo dictum. Mauris sit amet neque lacinia, eleifend lorem ut, tincidunt justo. Nulla mattis dolor id rhoncus volutpat. Nam aliquet augue non sapien malesuada euismod. Nunc ut mi cursus, sollicitudin velit feugiat, fermentum velit. Mauris vulputate feugiat suscipit. Suspendisse rutrum convallis eros, ut scelerisque nunc fringilla nec. Vestibulum fermentum eget est quis dictum.\r\n\r\nProin auctor ante sapien, et finibus lectus ultrices sed. Vivamus finibus tortor massa, vitae ullamcorper nisl efficitur at. Duis sed sapien eget libero efficitur imperdiet. Vivamus a sagittis mi. Sed pretium urna sed velit aliquet, ut pellentesque tortor euismod. Suspendisse id ipsum pulvinar, cursus enim interdum, molestie eros. Phasellus eget risus velit. Vivamus varius est ac quam tincidunt rhoncus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce efficitur vestibulum metus vitae pretium. Curabitur et fermentum justo, a venenatis ligula. Nullam vel dapibus purus. Morbi sit amet justo non ligula tincidunt aliquam nec quis ante. Morbi turpis felis, placerat in rutrum et, elementum ut magna. Vivamus sed ex eleifend ipsum pellentesque tristique at et nunc.\r\n\r\nMauris hendrerit tellus eget lectus feugiat maximus. Pellentesque quis velit hendrerit, condimentum mi sagittis, rhoncus justo. Vivamus accumsan, risus eu tempus scelerisque, mi purus placerat dui, vel hendrerit augue sapien ac odio. Mauris rutrum nulla nunc, vitae sagittis enim faucibus id. Proin blandit risus ac orci accumsan rutrum varius eu sapien. Mauris sed velit maximus, maximus augue vel, sodales elit. Duis elementum accumsan gravida. Quisque orci justo, euismod id neque vitae, aliquet sagittis felis. Vivamus ornare augue vel lacus feugiat lacinia ac id nibh. Duis et elit tempus, convallis magna a, interdum mi. Maecenas porta erat velit, non feugiat est faucibus quis.\r\n\r\nSed gravida vitae tellus at dignissim. Suspendisse placerat, lorem a finibus malesuada, dolor nisl laoreet sem, sed mollis erat magna vitae augue. Vestibulum viverra non purus non suscipit. Phasellus pretium fermentum lorem, non bibendum eros placerat vitae. Phasellus varius venenatis purus, faucibus ullamcorper risus gravida id. Pellentesque id consequat dolor, ut interdum ex. Cras suscipit quam nisl, sit amet venenatis nisi vulputate quis. Fusce eleifend eget eros ut tempor.','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut in risus eros. Quisque pellentesque rhoncus consectetur. Sed fringilla ante a velit dictum elementum. In semper maximus sapien non volutpat. Mauris lacus eros, luctus et hendrerit et, rhoncus at quam. Morbi tristique sapien in aliquam fringilla. Etiam non elementum purus, vitae varius odio.\r\n\r\nIn hendrerit imperdiet tortor sed consectetur. Nulla non dolor eu ex dapibus condimentum. Aenean congue, sapien vel tempus mattis, lacus ante ornare turpis, id efficitur felis orci id felis. Vestibulum sagittis sed turpis ac varius. Donec maximus tortor vitae orci aliquam ultricies id eget lectus. Donec vel pretium nulla. Ut blandit urna sed nibh commodo dictum. Mauris sit amet neque lacinia, eleifend lorem ut, tincidunt justo. Nulla mattis dolor id rhoncus volutpat. Nam aliquet augue non sapien malesuada euismod. Nunc ut mi cursus, sollicitudin velit feugiat, fermentum velit. Mauris vulputate feugiat suscipit. Suspendisse rutrum convallis eros, ut scelerisque nunc fringilla nec. Vestibulum fermentum eget est quis dictum.\r\n\r\nProin auctor ante sapien, et finibus lectus ultrices sed. Vivamus finibus tortor massa, vitae ullamcorper nisl efficitur at. Duis sed sapien eget libero efficitur imperdiet. Vivamus a sagittis mi. Sed pretium urna sed velit aliquet, ut pellentesque tortor euismod. Suspendisse id ipsum pulvinar, cursus enim interdum, molestie eros. Phasellus eget risus velit. Vivamus varius est ac quam tincidunt rhoncus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce efficitur vestibulum metus vitae pretium. Curabitur et fermentum justo, a venenatis ligula. Nullam vel dapibus purus. Morbi sit amet justo non ligula tincidunt aliquam nec quis ante. Morbi turpis felis, placerat in rutrum et, elementum ut magna. Vivamus sed ex eleifend ipsum pellentesque tristique at et nunc.\r\n\r\nMauris hendrerit tellus eget lectus feugiat maximus. Pellentesque quis velit hendrerit, condimentum mi sagittis, rhoncus justo. Vivamus accumsan, risus eu tempus scelerisque, mi purus placerat dui, vel hendrerit augue sapien ac odio. Mauris rutrum nulla nunc, vitae sagittis enim faucibus id. Proin blandit risus ac orci accumsan rutrum varius eu sapien. Mauris sed velit maximus, maximus augue vel, sodales elit. Duis elementum accumsan gravida. Quisque orci justo, euismod id neque vitae, aliquet sagittis felis. Vivamus ornare augue vel lacus feugiat lacinia ac id nibh. Duis et elit tempus, convallis magna a, interdum mi. Maecenas porta erat velit, non feugiat est faucibus quis.\r\n\r\nSed gravida vitae tellus at dignissim. Suspendisse placerat, lorem a finibus malesuada, dolor nisl laoreet sem, sed mollis erat magna vitae augue. Vestibulum viverra non purus non suscipit. Phasellus pretium fermentum lorem, non bibendum eros placerat vitae. Phasellus varius venenatis purus, faucibus ullamcorper risus gravida id. Pellentesque id consequat dolor, ut interdum ex. Cras suscipit quam nisl, sit amet venenatis nisi vulputate quis. Fusce eleifend eget eros ut tempor.','2020-12-19 09:48:09',NULL,NULL);

/*Table structure for table `undangans` */

DROP TABLE IF EXISTS `undangans`;

CREATE TABLE `undangans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sambutan_id` int(11) DEFAULT NULL,
  `harga_id` int(11) DEFAULT NULL,
  `subdomain` varchar(255) DEFAULT NULL,
  `guy_nickname` varchar(255) DEFAULT NULL,
  `girl_nickname` varchar(255) DEFAULT NULL,
  `datetime_start` datetime DEFAULT NULL,
  `datetime_end` datetime DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `google_maps` text DEFAULT NULL,
  `whatsapp_number` varchar(255) DEFAULT NULL,
  `payment_proof` varchar(255) DEFAULT NULL,
  `status` enum('0','1','2','3','4') DEFAULT NULL,
  `verified_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sambutan_id` (`sambutan_id`),
  KEY `harga_id` (`harga_id`),
  CONSTRAINT `undangans_ibfk_1` FOREIGN KEY (`sambutan_id`) REFERENCES `sambutans` (`id`),
  CONSTRAINT `undangans_ibfk_2` FOREIGN KEY (`harga_id`) REFERENCES `hargas` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `undangans` */

insert  into `undangans`(`id`,`sambutan_id`,`harga_id`,`subdomain`,`guy_nickname`,`girl_nickname`,`datetime_start`,`datetime_end`,`address`,`google_maps`,`whatsapp_number`,`payment_proof`,`status`,`verified_at`,`created_at`,`updated_at`,`deleted_at`) values 
(1,1,1,'widiana','widiana','anya','2020-12-19 10:30:01','2020-12-20 10:30:03','Jalan Sedap Malam Gang Melati Nomor 15 Denpasar','https://goo.gl/maps/BuEW4ds6KipceUFc6','082146456432','logo.png','3',NULL,'2020-12-19 12:27:09','2020-12-19 03:54:23',NULL);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` enum('0','1') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`email_verified_at`,`password`,`role`,`remember_token`,`created_at`,`updated_at`) values 
(1,'widiana','widi@me.com',NULL,'$2y$10$SXND1zTKY4deyjx2OxTM6.zncPDdRnQ.fPqmh/.w9a3tbJ161uHqC','1',NULL,'2020-12-18 18:06:36','2020-12-18 18:06:36');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
