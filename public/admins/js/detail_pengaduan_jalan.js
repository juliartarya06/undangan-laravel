var coordStr = [];
var poly;
var map, infoWindow;
var idJalan = $('#idJalan').val();
var latitude = parseFloat($('#latitude').val());
var longitude = parseFloat($('#longitude').val());
var jalan = $('#namaJalan').val();
var deskripsi = $('#deskripsi').val();
var image = $('#imgPengaduan').attr('src');
var markerPosition = { lat: latitude, lng: longitude };
var data = [];
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
//maps
function initMap() {
    var geocoder, marker;
    var input = document.getElementById('search');

    geocoder = new google.maps.Geocoder();
    infoWindow = new google.maps.InfoWindow;

    map = new google.maps.Map(document.getElementById('mapKu'), {
        disableDefaultUI: false,
        styles: styles,
        center: { lat: latitude, lng: longitude },
        zoom: 18,
        mapTypeControl: true,

    });

    // marker = new google.maps.Marker({
    //     map: map,
    //     draggable: true,
    //     animation: google.maps.Animation.DROP,
    // });

    infoWindow = new google.maps.InfoWindow;

    marker = new google.maps.Marker({
        position: { lat: latitude, lng: longitude },
        map: map
    });
    markerListener();
    google.maps.event.addListener(marker, 'click', markerListener)


    function markerListener() {
        infoWindow.setPosition(markerPosition);
        infoWindow.setContent("<b>Foto</b><br><div style='text-align:center'><img class='rounded' height='200px' alt='...' width = 'auto' src=" + image + "></div><br>" +
            "<b>alamat : </b>" + jalan +
            "<br><br><b>Deskripsi : </b>" + deskripsi +
            "<br><hr><button class='btn btn-primary btn-block' data-toggle='modal' data-target='#myModal' id='streetView'> <i class='fas fa-street-view'></i> Street View</button>");
        infoWindow.open(map);
        // map.setZoom(15);
        // map.setCenter(response.latLng);
        $(document).on('click', '#streetView', function() {
            panoramaView(markerPosition);
        });

    }

    // loadData();
}

function panoramaView(myLatLng) {
    var panorama = new google.maps.StreetViewPanorama(
        document.getElementById('pano'), {
            pov: {
                heading: 240,
                pitch: 0
            },
            visible: true
        });
    panorama.setPosition(myLatLng);
}
var styles = [{
    "featureType": "poi",
    "stylers": [
        { "visibility": "off" }
    ]
}]