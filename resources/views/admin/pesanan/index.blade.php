@extends('layouts.layout')
@section('title','Pesanan')
@section('content')
    <div class="row pt-2">
        <div class="col-md-12">
            <div class="card card-green">
                <div class="card-header">
                    <h3 class="card-title">Pesanan</h3>
                </div>

                <div class="card-body ">
                    <div class="table-responsive">
                        <table id="table" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>Link</th>
                                <th>Mempelai Pria</th>
                                <th>Mempelai Wanita</th>
                                <th>Status</th>
                                <th>Tanggal Dibuat</th>
                                <th>Bukti Pembayaran</th>
                                <th>Ubah Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($data as $m)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>widianapw.com/{{ $m->subdomain }}</td>
                                    <td>{{ $m->guy_nickname }}</td>
                                    <td>{{ $m->girl_nickname }}</td>
                                    <td>
                                        {{\App\Pesanan::getOrderStatus($m->status)}}
                                    </td>
                                    <td>@dateFormat($m->created_at)</td>
                                    <td>
                                        <center>
                                            <button data-toggle="modal" data-target="#exampleModal"
                                                    id="btnSeePaymentProof" class="btn btn-primary">
                                                <i class="fa fa-eye"></i>
                                            </button>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <div class="btn-group">
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-success dropdown-toggle"
                                                            data-toggle="dropdown">
                                                        Aksi
                                                    </button>
                                                    <div class="dropdown-menu">
                                                        <a class="dropdown-item"
                                                           href="{{route("admin.pesanan.changeStatus",["id"=>$m->id])}}"
                                                           onclick="event.preventDefault(); document.getElementById('status').value='1'; document.getElementById('changeStatusForm').submit() ">Verifikasi</a>
                                                        <a class="dropdown-item"
                                                           href="{{route("admin.pesanan.changeStatus",["id"=>$m->id])}}"
                                                           onclick="event.preventDefault(); document.getElementById('status').value='2'; document.getElementById('changeStatusForm').submit() ">Link
                                                            Terkirim</a>
                                                        <a class="dropdown-item"
                                                           href="{{route("admin.pesanan.changeStatus",["id"=>$m->id])}}"
                                                           onclick="event.preventDefault(); document.getElementById('status').value='3'; document.getElementById('changeStatusForm').submit() ">
                                                            Selesai
                                                        </a>
                                                        <a class="dropdown-item"
                                                           href="{{route("admin.pesanan.changeStatus",["id"=>$m->id])}}"
                                                           onclick="event.preventDefault(); document.getElementById('status').value='4'; document.getElementById('changeStatusForm').submit() ">Tidak
                                                            Terverifikasi</a>

                                                        <form
                                                            id="changeStatusForm"
                                                            action="{{route("admin.pesanan.changeStatus",["id"=>$m->id])}}"
                                                            method="POST" style="display: none">
                                                            @csrf
                                                            @method("PUT")
                                                            <input type="hidden" id="status" name="status" value="1">
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </center>
                                    </td>

                                </tr>
                                <div class="modal fade" id="exampleModal" tabindex="-1"
                                     aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Bukti Pembayaran</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <img class="img-fluid rounded mx-auto d-block "
                                                     src="{{asset('img/payment/'.$m->payment_proof)}}">
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-primary" data-dismiss="modal">
                                                    Tutup
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('js')
    <script>
        $(document).ready(function () {
            $('#table').DataTable();
        });
    </script>
@endsection
